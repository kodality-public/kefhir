/*
 * MIT License
 *
 * Copyright (c) 2024 Kodality
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.kodality.kefhir.rest;

import com.kodality.kefhir.core.exception.FhirException;
import com.kodality.kefhir.rest.exception.FhirExceptionHandler;
import com.kodality.kefhir.rest.filter.KefhirRequestFilter;
import com.kodality.kefhir.rest.filter.KefhirResponseFilter;
import com.kodality.kefhir.rest.model.KefhirRequest;
import com.kodality.kefhir.rest.model.KefhirResponse;
import com.kodality.kefhir.structure.api.ResourceContent;
import com.kodality.kefhir.structure.service.ContentTypeService;
import com.kodality.kefhir.structure.service.ResourceFormatService;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Put;
import jakarta.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r5.model.OperationOutcome.IssueType;
import org.hl7.fhir.r5.model.Resource;

@Consumes("*/*")
@Produces("*/*")
@Controller("/" + RuleThemAllFhirController.FHIR_ROOT)
@RequiredArgsConstructor
public class RuleThemAllFhirController {
  private static final String PRETTY = "_pretty";
  public static final String FHIR_ROOT = "fhir";
  private final KefhirEndpointService endpointService;
  private final ResourceFormatService resourceFormatService;
  private final ContentTypeService contentTypeService;
  private final List<KefhirRequestFilter> requestFilters;
  private final List<KefhirResponseFilter> responseFilters;
  private final FhirExceptionHandler fhirExceptionHandler;
  private final ServerUriHelper serverUriHelper;

  @PostConstruct
  public void init() {
    requestFilters.sort(Comparator.comparing(KefhirRequestFilter::getOrder));
    responseFilters.sort(Comparator.comparing(KefhirResponseFilter::getOrder));
  }

  private HttpResponse<?> execute(HttpRequest request) {
    try {
      KefhirRequest req = buildKefhirRequest(request);
      req.setOperation(endpointService.findOperation(req));
      try {
        requestFilters.forEach(f -> f.handleRequest(req));
        KefhirResponse resp = endpointService.execute(req);
        responseFilters.forEach(f -> f.handleResponse(resp, req));
        return readKefhirResponse(resp, req);
      } catch (Exception ex) {
        responseFilters.forEach(f -> f.handleException(ex, req));
        throw ex;
      }
    } catch (Exception ex) {
      return fhirExceptionHandler.handle(request, ex);
    }
  }

  private HttpResponse<String> readKefhirResponse(KefhirResponse resp, KefhirRequest req) {
    MutableHttpResponse<String> r = HttpResponse.status(HttpStatus.valueOf(resp.getStatus()));
    resp.getHeaders().forEach((k, vv) -> vv.stream().filter(Objects::nonNull).forEach(v -> r.header(k, v)));
    if (resp.getBody() != null) {
      List<String> accepts = req.getAccept().stream().map(MediaType::getName).toList();
      String accept = resourceFormatService.findSupported(accepts).get(0);
      ResourceContent formatted = format(resp.getBody(), accept);
      if ("true".equals(req.getParameter(PRETTY))) {
        prettify(formatted);
      }
      r.body(formatted.getValue());
      r.contentType(accept + ";charset=utf-8");
    }
    return r;
  }

  private ResourceContent format(Object body, String contentType) {
    if (ResourceContent.class.isAssignableFrom(body.getClass())) {
      ResourceContent content = (ResourceContent) body;
      if (contentType == null || contentTypeService.isSameType(contentType, content.getContentType())) {
        return content;
      }
      Resource resource = resourceFormatService.parse(content.getValue());
      return resourceFormatService.compose(resource, contentType);
    }
    if (Resource.class.isAssignableFrom(body.getClass())) {
      Resource resource = (Resource) body;
      return resourceFormatService.compose(resource, contentType);
    }
    throw new FhirException(500, IssueType.PROCESSING, "cannot write " + body.getClass());
  }

  private void prettify(ResourceContent content) {
    content.setValue(resourceFormatService.findPresenter(content.getContentType()).get().prettify(content.getValue()));
  }

  private KefhirRequest buildKefhirRequest(HttpRequest<String> request) {
    KefhirRequest req = new KefhirRequest();
    req.setServerUri(serverUriHelper.buildServerUri(request));
    req.setServerHost(serverUriHelper.getHost(request));
    req.setMethod(request.getMethodName());
    String p = request.getPath();
    p = StringUtils.removeStart(p, "/");
    p = StringUtils.removeStart(p, serverUriHelper.getContextPath());
    p = StringUtils.removeStart(p, "/");
    p = StringUtils.removeStart(p, FHIR_ROOT);
    p = StringUtils.removeStart(p, "/");
    if (!p.isEmpty() && Character.isUpperCase(p.charAt(0))) { // a bit ugly way to understand is it a /root request or not
      req.setType(StringUtils.substringBefore(p, "/"));
      req.setPath(StringUtils.substringAfter(p, "/"));
    } else {
      req.setPath(p);
    }
    request.getHeaders().forEachValue(req::putHeader);
    request.getParameters().forEachValue(req::putParameter);
    req.setBody(request.getBody(String.class).orElse(null));
    return req;
  }

  @Get(uris = {"{path:.*}"})
  public HttpResponse<?> get(HttpRequest request) {
    return execute(request);
  }

  @Post(uris = {"{path:.*}"})
  public HttpResponse<?> post(HttpRequest request, @Nullable @Body String json) {
    return execute(request);
  }

  @Put(uris = {"{path:.*}"})
  public HttpResponse<?> put(HttpRequest request, @Nullable @Body String json) {
    return execute(request);
  }

  @Delete(uris = {"{path:.*}"})
  public HttpResponse<?> delete(HttpRequest request) {
    return execute(request);
  }

}
