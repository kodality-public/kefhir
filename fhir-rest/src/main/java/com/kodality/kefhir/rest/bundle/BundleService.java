/*
 * MIT License
 *
 * Copyright (c) 2024 Kodality
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.kodality.kefhir.rest.bundle;

import com.kodality.kefhir.core.exception.FhirException;
import com.kodality.kefhir.core.model.search.SearchCriterion;
import com.kodality.kefhir.core.model.search.SearchCriterionBuilder;
import com.kodality.kefhir.core.model.search.SearchResult;
import com.kodality.kefhir.core.service.resource.ResourceSearchService;
import com.kodality.kefhir.rest.KefhirEndpointService;
import com.kodality.kefhir.rest.ServerUriHelper;
import com.kodality.kefhir.rest.model.KefhirRequest;
import com.kodality.kefhir.rest.model.KefhirResponse;
import com.kodality.kefhir.rest.util.PreferredReturn;
import com.kodality.kefhir.structure.api.ResourceContent;
import com.kodality.kefhir.structure.service.ResourceFormatService;
import com.kodality.kefhir.tx.TransactionService;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MediaType;
import io.micronaut.http.context.ServerRequestContext;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r5.model.Bundle;
import org.hl7.fhir.r5.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r5.model.Bundle.BundleEntryResponseComponent;
import org.hl7.fhir.r5.model.Bundle.BundleType;
import org.hl7.fhir.r5.model.Bundle.HTTPVerb;
import org.hl7.fhir.r5.model.Bundle.LinkRelationTypes;
import org.hl7.fhir.r5.model.Extension;
import org.hl7.fhir.r5.model.OperationOutcome;
import org.hl7.fhir.r5.model.OperationOutcome.IssueType;
import org.hl7.fhir.r5.model.Resource;
import org.hl7.fhir.r5.model.UriType;

@Singleton
@RequiredArgsConstructor
public class BundleService implements BundleSaveHandler {
  private final ResourceSearchService searchService;
  private final BundleReferenceHandler bundleReferenceHandler;
  private final KefhirEndpointService endpointService;
  private final ResourceFormatService resourceFormatService;
  private final TransactionService tx;
  private final ServerUriHelper serverUriHelper;

  @Override
  public Bundle save(Bundle bundle, String prefer) {
    if (bundle.getEntry().stream().anyMatch(e -> !e.hasRequest())) {
      throw new FhirException(400, IssueType.INVALID, "Bundle.request element required");
    }

    if (bundle.getType() == BundleType.BATCH) {
      bundle.getEntry().sort(new EntityMethodOrderComparator());
      return batch(bundle, prefer);
    }
    if (bundle.getType() == BundleType.TRANSACTION) {
      validateTransaction(bundle);
      bundleReferenceHandler.replaceIds(bundle);
      bundle.getEntry().sort(new EntityMethodOrderComparator()); //moved after replaceIds because incorrect behavior in case of conditional updates
      return transaction(bundle, prefer);
    }
    throw new FhirException(400, IssueType.INVALID, "only batch or transaction supported");
  }

  private void validateTransaction(Bundle bundle) {
    bundle.getEntry().forEach(entry -> {
      if (entry.getRequest().getMethod() == HTTPVerb.POST && entry.getRequest().getIfNoneExist() != null) {
        String ifNoneExist = entry.getRequest().getIfNoneExist() + "&_count=0";
        String type = entry.getResource().getResourceType().name();
        SearchCriterion criteria = SearchCriterionBuilder.parse(ifNoneExist, type);
        SearchResult result = searchService.search(criteria);
        if (result.getTotal() == 1) {
          entry.getRequest().setMethod(HTTPVerb.NULL); //ignore
        }
        if (result.getTotal() > 1) {
          String msg = "was expecting 0 or 1 resources. found " + result.getTotal();
          throw new FhirException(412, IssueType.PROCESSING, msg);
        }
      }
    });
  }

  private Bundle batch(Bundle bundle, String prefer) {
    Bundle responseBundle = new Bundle();
    bundle.getEntry().forEach(entry -> {
      try {
        responseBundle.addEntry(perform(entry, prefer));
      } catch (Exception e) {
        FhirException fhirException = findFhirException(e);
        if (fhirException != null) {
          BundleEntryResponseComponent responseEntry = new BundleEntryResponseComponent();
          responseEntry.setStatus("" + fhirException.getStatusCode());
          BundleEntryComponent responseBundleEntry = responseBundle.addEntry();
          responseBundleEntry.addLink().setRelation(LinkRelationTypes.ALTERNATE).setUrl(entry.getFullUrl());
          OperationOutcome outcome = new OperationOutcome();
          outcome.setIssue(fhirException.getIssues());
          responseBundleEntry.setResource(outcome);
          responseBundleEntry.setResponse(responseEntry);
          return;
        }
        throw new RuntimeException("entry: " + entry.getFullUrl(), e);
      }
    });
    responseBundle.setType(BundleType.BATCHRESPONSE);
    return responseBundle;
  }

  private Bundle transaction(Bundle bundle, String prefer) {
    return tx.transaction(() -> {
      Bundle responseBundle = new Bundle();
      bundle.getEntry().stream().forEach(entry -> {
        try {
          responseBundle.addEntry(perform(entry, prefer));
        } catch (Exception e) {
          FhirException fhirException = findFhirException(e);
          if (fhirException != null) {
            fhirException.addExtension("fullUrl", entry.getFullUrl());
            fhirException.getIssues().forEach(i -> {
              String expr = "Bundle.entry[" + bundle.getEntry().indexOf(entry) + "]";
              i.addExpression(expr);
            });
          }
          throw e;
        }
      });
      responseBundle.setType(BundleType.TRANSACTIONRESPONSE);
      return responseBundle;
    });
  }

  private BundleEntryComponent perform(BundleEntryComponent entry, String prefer) {
    if (entry.getRequest().getMethod() == HTTPVerb.NULL) {
      //XXX hack  @see #validateTransaction
      return new BundleEntryComponent().setResponse(new BundleEntryResponseComponent().setStatus("200"));
    }
    KefhirRequest req = buildRequest(entry);
    if (StringUtils.isEmpty(req.getType())) {
      throw new FhirException(400, IssueType.PROCESSING, "what are you trying to do, mister?");
    }
    req.setOperation(endpointService.findOperation(req));
    KefhirResponse resp = endpointService.execute(req);

    BundleEntryComponent newEntry = new BundleEntryComponent();
    newEntry.setResponse(new BundleEntryResponseComponent().setStatus(resp.getStatus().toString()).setLocation(getLocation(resp)));
    if (resp.getBody() != null && resp.getBody() instanceof Resource) {
      newEntry.setResource((Resource) resp.getBody());
    }
    if (resp.getBody() != null && resp.getBody() instanceof ResourceContent) {
      newEntry.setResource(resourceFormatService.parse((ResourceContent) resp.getBody()));
    }
    if (entry.getFullUrl() != null) {
      newEntry.addLink().setRelation(LinkRelationTypes.ALTERNATE).setUrl(entry.getFullUrl());
    }
    if (StringUtils.equals(prefer, PreferredReturn.OperationOutcome)) {
      newEntry.getResponse().setOutcome(new OperationOutcome());
    }
    return newEntry;
  }

  private KefhirRequest buildRequest(BundleEntryComponent entry) {
    KefhirRequest req = new KefhirRequest();
    HttpRequest<Object> request = ServerRequestContext.currentRequest().orElseThrow();
    req.setServerUri(serverUriHelper.buildServerUri(request));
    req.setServerHost(serverUriHelper.getHost(request));
    String method = entry.getRequest().getMethod().toCode();
    req.setTransactionMethod(method);
    URI uri;
    Extension transactionGeneratedId = entry.getRequest().getExtensionByUrl("urn:kefhir-transaction-generated-id");
    if (method.equals("POST") && transactionGeneratedId != null) {
      req.setMethod("PUT");
      uri = URI.create(((UriType) transactionGeneratedId.getValue()).getValue());
    } else {
      req.setMethod(method);
      String url = entry.getRequest().getUrl();
      url = url.replaceAll("\\|", URLEncoder.encode("|", StandardCharsets.UTF_8));
      uri = URI.create(url);
    }
    req.setType(StringUtils.substringBefore(uri.getPath(), "/"));
    req.setPath(StringUtils.removeStart(uri.getPath(), req.getType()));
    req.putQuery(uri.getQuery());
    req.putHeader("If-None-Exist", entry.getRequest().getIfNoneExist());
    req.setContentType(MediaType.APPLICATION_JSON_TYPE);
    if (entry.getResource() != null) {
      req.setBody(resourceFormatService.compose(entry.getResource(), "json").getValue());
    }
    return req;
  }

  private String getLocation(KefhirResponse resp) {
    if (resp.getHeader("Content-Location") != null) {
      return resp.getHeader("Content-Location");
    }
    if (resp.getHeader("Location") != null) {
      return resp.getHeader("Location");
    }
    return null;
  }

  private FhirException findFhirException(Throwable e) {
    if (e instanceof FhirException) {
      return (FhirException) e;
    }
    if (e.getCause() != null) {
      return findFhirException(e.getCause());
    }
    return null;
  }

}
