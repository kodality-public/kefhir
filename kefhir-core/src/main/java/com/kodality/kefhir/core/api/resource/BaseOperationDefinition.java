package com.kodality.kefhir.core.api.resource;

public interface BaseOperationDefinition {
  String getResourceType();
  String getOperationName();
}
