package com.kodality.kefhir;

import io.micronaut.runtime.Micronaut;

public class KefhirTestApplication {

  public static void main(String[] args) {
    Micronaut.run(KefhirTestApplication.class);
  }
}
